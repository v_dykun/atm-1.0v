import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class AdminClass {

    public static final int passwordAdmin = 1234;
    public static final String loginAdmin = "admin";
    public static int atmBalance=100000;
    private int valueRepeatLogin=0;
    private int valueRepeatPassword=0;
    public static ArrayList<UserClass> crUser = new ArrayList<>();

// This method creates a list of users. When starting the program we work with this list.
    public static void createUserList() {
        UserClass user1 = new UserClass("user1", 1111, 1000);
        UserClass user2 = new UserClass("user2", 2222, 2000);
        UserClass user3 = new UserClass("user3", 3333, 3000);
        crUser.add(user1);
        crUser.add(user2);
        crUser.add(user3);
    }

// In this method, we add a user in list. This user was created using the method createUser(). The method returns a list.
    public static ArrayList<UserClass> addUserToList() {
        UserClass user = createUser();
        crUser.add(user);
        System.out.println("New user added for base");
        return crUser;
    }

// The first menu when you start the program.
    public void startMenu() {
        createUserList();
        System.out.println("Welcome to ATM. Make your choice:");
        System.out.println("1) Admin service " + "\n" + "2) Client service " + "\n" + "3) Exit ");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                enterAdmin();
                break;
            case 2:
                findUser();
                break;
            case 3:
                exit();
                break;
            default:
                System.out.println("This option does not exist!");
                break;
        }
    }

// The method checks the correct login and password. Uses methods checkLogin() and checkPassword().
    public void enterAdmin() {
        if (checkLogin()){
            if (checkPassword()) {
                adminMenu();
            } else { System.out.println("The password does not match the login Admin");
                exitAdmin();
            }
        } else {
            exitAdmin();
        }
    }

// The method takes the login and compares it with loginAdmin. Gives three attempts to enter the login.
    public boolean checkLogin() {
        System.out.println("Enter Login:");
        String login = new Scanner(System.in).nextLine();
        if (loginAdmin.equals(login)) {
            return true;
        } else {
            valueRepeatLogin++;
            if (valueRepeatLogin < 3) {
                System.out.println("You entered the wrong login");
                System.out.println("Enter your login again!");
                System.out.println();
               return checkLogin();
            } else {
                System.out.println("Exceeded the number of login attempts!");
                return false;
            }
        }
    }

//  The method takes the password and compares it with passwordAdmin. Gives three attempts to enter the password.
    public boolean checkPassword() {
        try {
            System.out.println("Enter Password For Admin:");
            int password = new Scanner(System.in).nextInt();
            if (passwordAdmin==password) {
                return true;
            } else {
                valueRepeatPassword++;
                if (valueRepeatPassword<3) {
                    System.out.println("You entered the wrong password for this login");
                    System.out.println("Enter your password again!");
                    System.out.println();
                    return checkPassword();
                } else {
                    System.out.println("Exceeded the number of Password attempts!");
                    return false;
                }
            }
        } catch (InputMismatchException e) {
            System.out.println("The admin password is only digits");
            return checkPassword();
        }
    }

// Menu after confirming login and password admin. Separates the following admin actions for items.
    public void adminMenu(){
        System.out.println("You are logged in as an admin, select the following action:");
        System.out.println("1) ATM balance " + "\n" + "2) Refill ATM balance " + "\n" + "3) Create user"  + "\n" + "4) Exit admin mode");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                printAtmBalance();
                endMenu();
                break;
            case 2:
                refillAtmBalance();
                endMenu();
                break;
            case 3:
                addUserToList();
                endMenu();
                break;
            case 4:
                exitAdmin();
                break;
            default:
                System.out.println("This option does not exist!");
                break;
        }
    }

// The method creates the user and returns it.
    public static UserClass createUser() {
        UserClass user = new UserClass();
       try {
           System.out.println("Enter user Login:");
           user.userLogin = new Scanner(System.in).nextLine();
           System.out.println("Enter the password for user Login:");
           user.userPassword = new Scanner(System.in).nextInt();
           user.userBalans = 0;

           System.out.println("User is create!");
       } catch (InputMismatchException e) {
           System.out.println("The password must consist of digits.");
           System.out.println("User not created. Start from the beginning.");
           System.out.println();
           createUser();
       }
        return user;
    }

// Brings balance ATM to the screen.
    public static void printAtmBalance () {
        System.out.println("ATM balance is: "+ atmBalance);
    }

// Reads the number from the keyboard and adds it to atmBalance.
    public static void refillAtmBalance() {
        try {
            System.out.println("Enter the amount of replenishment of the balance: ");
            int ball = new Scanner(System.in).nextInt();
            atmBalance = atmBalance +ball;
            System.out.println("New balance is: " + atmBalance);
        } catch (InputMismatchException e) {
            System.out.println("The amount should consist only of digits.");
            System.out.println();
            refillAtmBalance();
        }
    }

// Find a user on the list crUser and switch to work with the class UserClass
    public void findUser() {

        System.out.println("Enter Login for User:");
        String login = new Scanner(System.in).nextLine();

        for (int i=0;i<crUser.size();i++) {
            try {
                if (crUser.get(i).checkLogin(login)) {
                    System.out.println("Enter password for this User:");
                    int password = new Scanner(System.in).nextInt();

                    if (crUser.get(i).checkPassword(password)) {
                        UserClass user = crUser.get(i);
                        user.userMenu();
                        break;
                    } else {
                        System.out.println("This password does not match this login");
                    }
                } else {
                    if (i == crUser.size() - 1) {
                        System.out.println("You have entered the wrong login");
                        startMenu();
                    }
                }
            } catch (InputMismatchException e) {
                System.out.println("The user password is only digits");
                findUser();
            }

        }
    }

// Exit from user "Admin" and return to startMenu().
    public void exitAdmin() {
        System.out.println("Thank you for using admin mode!");
        startMenu();
    }

// Method to confirm your exit. Asking if you want to leave completely, or want to go to adminMenu().
    public void endMenu() {
        System.out.println("You want to leave Admin mode:");
        System.out.println("1) NO" + "\n" + "2) YES");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                adminMenu();
                break;
            case 2:
                exitAdmin();
                break;
            default:
                System.out.println("This option does not exist!");
                break;
        }
    }

// Full exit and ending with the program.
    public void exit () {
        System.out.println("Thank you for using ATM");
    }
}
