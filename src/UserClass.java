import java.util.Scanner;

public class UserClass {
    public String userLogin;
    public int userPassword;
    public int userBalans;

/*
    All methods will repeat the logic of the operation of methods from the class AdminClass
    and are executed over the selected user from the method findUser().
*/

    public UserClass() {
    }

    public UserClass(String user, int pas, int bal) {
        this.userLogin = user;
        this.userPassword = pas;
        this.userBalans = bal;
    }

    public boolean checkLogin(String log) {
        if (userLogin.equals(log)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkPassword(int pass) {
        if (userPassword==pass) {
            return true;
        } else {
            return false;
        }
    }

    public void userMenu () {
        System.out.println("You are logged in as an "+ userLogin +", select the following action:");
        System.out.println("1) You balance " + "\n" + "2) Put Cash " + "\n" + "3) Take Cash"  + "\n" + "4) Exit user mode");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                balance();
                endMenu();
                break;
            case 2:
                putCash();
                endMenu();
                break;
            case 3:
                takeOfCash();
                endMenu();
                break;
            case 4:
                exitUser();
                break;
            default:
                System.out.println("This option does not exist!");
                break;
        }
    }

    public  void balance() {
        System.out.println("Your balance:"+ userBalans);
    }

    public void takeOfCash() {
        System.out.println("Specify the amount you want to take: ");
        int cash = new Scanner(System.in).nextInt();
        if (userBalans<cash) {
            System.out.println("You do not have enough funds on the account.");
            balance();
            endMenu();
        } else {
            userBalans = userBalans - cash;
            balance();
        }

    }

    public void putCash() {
        System.out.println("Specify the amount you want to put: ");
        int cash = new Scanner(System.in).nextInt();
        userBalans = userBalans + cash;
        balance();
    }

    public void endMenu() {
        System.out.println("You want to leave User mode:");
        System.out.println("1) NO" + "\n" + "2) YES");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                userMenu();
                break;
            case 2:
                exitUser();
                break;
            default:
                System.out.println("This option does not exist!");
                break;
        }
    }

    public void exitUser () {
        System.out.println("Thank you for using ATM");
    }
}
